require 'net/http'
require 'json'
require 'acme-client'
require 'openssl'
require 'base64'
require 'resolv'
require 'fog/google'
require 'timeout'
require 'socket'

require_relative 'letsencrypt'
require_relative 'google'
require_relative 'cisco_asa'

# load credentials
require_relative '.secret'

# TODO: Move CloudFlare actions to separate class
# Execution timeout 30m
EXEC_TIMEOUT = 1800

CRT_EXP_BEFORE = 15

# CF API uri
API_URI = URI('https://api.cloudflare.com/client/v4/')
# DNS resolver
RES = Resolv::DNS.new(nameserver: ['8.8.8.8', '8.8.4.4'])

# Domain normalization regex
DOMAIN_NORM_REGX = /[^.]*\.[^.]+$/
# CONF structure
# CONF['acc_private_key'] str -LE acc private_key path
# CONF['private_key'] str -Domain private_key path
# CONF['email'] str -LE email address
# CONF['domains'] arr -Array of domains
# CONF['kid'] str   -LE account id
# CONF['staging'] bool true||false
# CONF['google_upload_skip'] bool true||false,
# CONF['save_pem'] bool true||false
# Cisco ASA only, can't be enabled with CF and google crt upload
# CONF['asa_crt_upload']: bool true||false
# CONF['asa_hostname']: "asa-mgmt",
# CONF['asa_username']: "cli-access_user",
# CONF['asa_password']: "super password"

ARGV.index('--conf') ? conf_path = ARGV[ARGV.index('--conf') + 1] : conf_path = 'conf.d/config.json'
CONF = JSON.parse(File.read(conf_path))

# Actions
GET = %w[get zones].freeze
GET_CRT = %w[get_crt custom_certificates?status=active&order=status&direction=desc].freeze
GET_ALL_DNS_REC = %w[get_dns_recs dns_records?per_page=100&direction=desc].freeze
CREATE_DNS = %w[create_dns dns_records].freeze
UPLOAD_CRT = %w[upload_crt custom_certificates].freeze
UPDATE_CRT = %w[update_crt custom_certificates].freeze

$zone_hash = {}

# Check if zone is found
def chk_zone
  unless $zone_hash.key?(@zone_name.to_sym)
    puts '{"error":"Zone not found"}'
    exit(127)
  end
end

def action(action_type, zone_name = nil, data = nil, obj_id = nil)
  @zone_name = zone_name
  @action_type = action_type[0]
  full_uri = "#{API_URI}zones/#{$zone_hash[zone_name.to_sym]}/#{action_type[1]}" if @zone_name

  if action_type[0] == 'get'
    req = Net::HTTP::Get.new(API_URI + action_type[1])
  elsif action_type[0] == 'get_crt'
    chk_zone
    req = Net::HTTP::Get.new(full_uri)
  elsif action_type[0] == 'get_dns_recs'
    chk_zone
    req = Net::HTTP::Get.new(full_uri)
  elsif action_type[0] == 'create_dns'
    chk_zone
    req = Net::HTTP::Post.new(full_uri)
  elsif action_type[0] == 'upload_crt'
    chk_zone
    req = Net::HTTP::Post.new(full_uri)
  elsif action_type[0] == 'update_crt'
    chk_zone
    req = Net::HTTP::Patch.new(full_uri + '/' + obj_id)
  else
    puts '> Unknown action was provided'
  end

  # Credentials
  req['X-Auth-Email'] = EMAIL
  req['X-Auth-Key'] = KEY
  req['Content-Type'] = 'application/json'

  req.body = data unless data.nil?

  Net::HTTP.start(API_URI.hostname, API_URI.port, use_ssl: API_URI.scheme == 'https') do |http|
    http.request(req) do |res|
      response = JSON.parse(res.body)
      abort(response.to_s) unless response['success']
      return response
    end
  end
end

# Get all zones ids, and save into @zone_hash
def zones_list
  puts '> Get zones'
  action(GET)['result'].each do |hash|
    $zone_hash[hash['name'].to_sym] = hash['id']
  end
end

def get_dns_ids(name)
  action(GET_ALL_DNS_REC, name).each do |arr|
    if arr[0] == 'result'
      arr[1].each do |arr|
        puts "#{arr['name']};#{arr['type']};#{arr['id']}"
      end
    end
  end
end

def pub_crt_get_info(domain_name)
  return unless CONF['asa_crt_upload']
  tcp_client = TCPSocket.new(domain_name, 443)
  ssl_client = OpenSSL::SSL::SSLSocket.new(tcp_client)
  ssl_client.hostname = domain_name
  ssl_client.connect
  cert = OpenSSL::X509::Certificate.new(ssl_client.peer_cert)
  ssl_client.sysclose
  tcp_client.close
  puts "Expiration_date: #{cert.not_after}"
  # exp days * 24h * 60m * 60s, convert days to sec
  cert.not_after <= Time.now + (CRT_EXP_BEFORE * 24 * 60 * 60)
end

def cf_crt_get_info(zone)
  action(GET_CRT, zone)['result'].each do |hash|
    @obj_id = hash['id']
    @crt_dns = hash['hosts']
    @crt_exp_date = hash['expires_on']
  end
end

def crt_expire?(zone)
  cf_crt_get_info(zone)
  return if @obj_id.nil?
  Date.strptime(@crt_exp_date, '%Y-%m-%d %H:%M:%S%Z') <= Date.today + CRT_EXP_BEFORE
end

# update or upload certificate to CloudFlare, update if 'obj_id' presented
def cf_crt_upload_update(private_key, public_key, zone, obj_id = nil)
  private = OpenSSL::PKey::RSA.new(File.read(private_key)).to_json
  body = "{\"certificate\":#{public_key},\"private_key\":#{private}}"
  if obj_id
    puts "> Update crt for #{zone}: #{action(UPDATE_CRT, zone, body, obj_id)}"
  else
    puts "> Upload crt for #{zone}: #{action(UPLOAD_CRT, zone, body)}"
  end
end

def domain_get_txt(record_name)
  RES.getresources(record_name, Resolv::DNS::Resource::IN::TXT)
end

def create_cf_dns_record(record_name, record_type, record_content)
  # extract zone name form domain
  zone = record_name[DOMAIN_NORM_REGX]
  counter = 0
  ready = false

  # test
  puts """
  rec_name #{record_name},
  rec_type #{record_type},
  rec_cont #{record_content}
  """

  # Get DNS record
  if domain_get_txt(record_name).any?
    ret = domain_get_txt(record_name)
    ret.each do |dns_record|
      if dns_record.strings[0] == record_content
        puts "> Skip, TXT record for #{record_name} exists."
        return 'The record exists.'
      end
    end
  end

  puts "> Add cf dns record: #{record_name}"
  body = "{\"type\":\"#{record_type}\",\"name\":\"#{record_name}\",\"content\":\"#{record_content}\",\"proxied\":false}"
  
  action(CREATE_DNS, zone, body)
  # Wait a few sec, new dns record doesn't appear so fast
  sleep 2
  until domain_get_txt(record_name).any?
    print "waiting for dns: #{record_name}, #{counter} sec. \r"
    counter += 1
    sleep 1
  end

  puts "> Make dns request for #{record_name}"
  # TODO: error on first start if request crt for domain.com and *.domain.com
  until ready
    puts "retry, get dns info... #{counter} sec. \r"
    # test p dns_record
    domain_get_txt(record_name).each do |dns_record|
      p dns_record.strings[0]
      dns_record.strings[0] == record_content ? ready = true : false
      sleep 2
    end
    counter += 2
  end
end

def delete_cf_dns_record
  raise NotImplementedError
end

# create arr with list of zones, remove duplicates
def normalize_domains(domains_arr = [])
  domains_arr.map { |i| i[DOMAIN_NORM_REGX] }.uniq
end

def get_crt
  le_crt = GetLetsEncrypt.new(CONF['acc_private_key'], CONF['private_key'], CONF['domains'].split(' '), CONF['email'], CONF['kid'])
  client = CONF['staging'] ? le_crt.staging_client : le_crt.prod_client
  begin
    le_crt.sign_le_certificate(le_crt.authorization(client))
  rescue Acme::Client::Error::AccountDoesNotExist
    puts '> Account not found, registering...'
    le_crt.register(client)
    le_crt.sign_le_certificate(le_crt.authorization(client))
  rescue StandardError => e
    puts e.message
    puts e.backtrace.inspect
  end
end

Timeout.timeout(EXEC_TIMEOUT) do
  # EXEC PART starts here
  # Get zones from CF or Google
  zone = CONF['domains'].split(' ')[0][DOMAIN_NORM_REGX]
  google_crt_name = zone.tr('.', '-')
  if CONF['dns_provider'] == 'google'
    GCloud.get_zones
  elsif CONF['dns_provider'] == 'CloudFlare'
    zones_list
  else
    raise NotImplementedError
  end

  puts """
    ###
    ## Start time: #{Time.now}
    ###
  """
  # if @crt_exp_date is nil consider it as no crt was uploaded
  pub_crt = get_crt if crt_expire?(zone) || @crt_exp_date.nil? || pub_crt_get_info(CONF['domains'].first)

  if CONF['asa_crt_upload']
    abort('Certificate valid.') unless pub_crt_get_info(CONF['domains'].first)
    # Disable ssl verification (asa mgmt crt is self signed)
    OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
    asa = CiscoASA.new(CONF['private_key'], pub_crt, CONF['asa_username'], CONF['asa_password'],
                       CONF['asa_hostname'], CONF['domains'])
    asa.upload_crt(asa.convert_to_p12)
    asa.pin_crt
    exit(0)
  end

  if CONF['dns_provider'] == 'CloudFlare'
    nothing_to_update = true
    normalize_domains(CONF['domains'].split(' ')).each do |zone|
      @crt_exp_date = '1987-03-29 00:00:01Z' if cf_crt_get_info(zone).empty?
      if Date.strptime(@crt_exp_date, '%Y-%m-%d %H:%M:%S%Z') <= Date.today + CRT_EXP_BEFORE
        nothing_to_update = false
        cf_crt_upload_update(CONF['private_key'], pub_crt, zone, @obj_id)
      else
        puts '> ' + zone
        puts "Nothing to update, certificate valid til: #{@crt_exp_date}"
      end
    end

    exit(0) if nothing_to_update || CONF['google_upload_skip']

    google_crt = GCloud.new(CONF['google_project'], CONF['google_json_key_location'], CONF['private_key'],
                            pub_crt, google_crt_name)
    google_crt.upload_crt
    google_crt.set_lb_ssl
  end

  if CONF['dns_provider'] == 'google'
    raise NotImplementedError
  end
end
