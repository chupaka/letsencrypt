# Upload crt to GCP and set it as default
class GCloud

  def initialize(google_project, json_key, private_key, public_key, crt_name, dns_zone = "")
    @google_project = google_project
    @json_key = json_key
    @private_key = File.read(private_key)
    @public_key = public_key
    @crt_name = crt_name + '-' + Time.now.to_i.to_s
    @dns_zone = dns_zone if CONF['dns_provider'] == 'google' or 'Google'
  end

  def connect(type)
      eval("Fog::#{type}::Google.new(google_project: @google_project, google_json_key_location: @json_key)")
  end

  # replace eval with something
  def upload_crt
    puts "> Upload crt: #{@crt_name} to #{@google_project}"
    connect('Compute').insert_ssl_certificate(@crt_name, eval(@public_key), @private_key)
    # wait a few sec for resource being ready
    sleep 3
  end

  def set_lb_ssl
    puts "> Set target proxy: #{CONF['google_lb']}, crt: #{@crt_name}"
    crt_uri = ["https://www.googleapis.com/compute/v1/projects/#{@google_project}/global/sslCertificates/#{@crt_name}"]
    connect('Compute').set_target_https_proxy_ssl_certificates(CONF['google_lb'], crt_uri)
  end

  def get_zones
    connect('DNS').list_managed_zones.managed_zones.each do |i|
      $zone_hash[i.dns_name] = i.name
    end
  end

  def add_gcp_dns(record_name, record_type, record_content)
    zone = connect('DNS').zones.get($zone_hash[record_name + '.'])
    p zone
    zone.records.create(name: (record_name + '.'), type: record_type, ttl: 600, rrdatas: [record_content])
  end
  
  def update_gcp_dns(record_name, record_type, record_content)
    zone = connect('DNS').zones.get($zone_hash[record_name + '.'])
    record = connect('DNS').records(zone: zone).get(record_name + '.', record_type)
    # Get existing dn records and remove double quotes
    exRec = record.rrdatas.map { |i| i.gsub(/\"/, '')}
    exRec.push(record_content)
    record.modify(rrdatas: exRec)  
  end
  
  def destroy_dns(record_name, record_type)
    zone = connect('DNS').zones.get($zone_hash[record_name + '.'])
    record = connect('DNS').records(zone: zone).get(record_name + '.', record_type)
    p record
    record.destroy
  end
end

# get_zones
# DOMAIN_NORM_REGX = /[^.]*\.[^.]+$/
# a = 0
# normalize_domains("gcp-li.ga *.gcp-li.ga *.minsk.gcp-li.ga".split(' ')).each do |i|
#   a += 1
#   zone = connect('DNS').zones.get($zone_hash[i + '.'])
#   if connect('DNS').records(zone: zone).get((i + '.'), 'TXT').nil?
#     add_gcp_dns(i, 'TXT', 'KSDASDSALKKKC7asYYxz' + a.to_s)
#   else
#     update_gcp_dns(i, 'TXT', 'KSDASDSALKKKC7asYYxz' + a.to_s)
#   end
# end